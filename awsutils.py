#!/usr/bin/env python
import optparse
import boto3
import csv


def write_csv_file(file_name, data):
    first_line = True
    with open(file_name, 'w') as file:
        for item in data:
            try:
                keys = item.keys()
                writer = csv.DictWriter(file, keys)
                if first_line:
                    writer.writeheader()
                    first_line = False
                else:
                    writer.writerow(item)
            except Exception as e:
                print('Write data to csv file failed, %s', e)


def account_parser(file_name):
    account_list = []
    with open(file_name, 'r') as f:
        try:
            account_info = eval(f.read())
            for account in account_info['accounts']:
                if 'AccesskeyID' not in account or 'Secretaccesskey' not in account:
                    continue
                account_info = {}
                account_info['key_id'] = account['AccesskeyID'].strip()
                account_info['secret_key'] = account['Secretaccesskey'].strip()
                account_list.append(account_info)
        except Exception as e:
            print('This is not the key file', e)
            raise Exception
    return account_list


class AWSUtils():

    def __init__(self, key_id, secret_key):
        self.key_id = key_id
        self.secret_key = secret_key

    def __get_account_id(self):

        sts = boto3.client(
            "sts", aws_access_key_id=self.key_id, aws_secret_access_key=self.secret_key)
        user_arn = sts.get_caller_identity()["Arn"]
        account_id = user_arn.split(":")[4]
        return account_id

    def __get_all_regsions(self):
        ec2 = boto3.client(
            'ec2', aws_access_key_id=self.key_id, aws_secret_access_key=self.secret_key)

        all_regions = ec2.describe_regions()
        region_list = []
        for region in all_regions['Regions']:
            region_list.append(region['RegionName'])
        return region_list

    def get_instances(self):
        all_instacne = []
        region_list = self.__get_all_regsions()
        account_id = self.__get_account_id()
        print('Get instances from account id %s' % account_id)

        for region in region_list:

            ec2 = boto3.client(
                'ec2', aws_access_key_id=account['key_id'], aws_secret_access_key=account['secret_key'], region_name=region)

            all_az = ec2.describe_availability_zones()
            az_list = []
            for az in all_az['AvailabilityZones']:
                az_list.append(az['ZoneName'])

            instances = ec2.describe_instances(
                Filters=[{'Name': 'availability-zone', 'Values': az_list}])
            num_of_instance = 0
            for instance in instances['Reservations']:
                for one in instance['Instances']:
                    instance_info = {}
                    instance_info['account_id'] = account_id
                    if 'VpcId' in one:
                        instance_info['VpcId'] = one['VpcId']
                    if 'InstanceType' in one:
                        instance_info['InstanceType'] = one['InstanceType']
                    if 'Tags' in one:
                        for tag in one['Tags']:
                            if tag['Key'] == 'Name':
                                instance_info['Name'] = tag['Value']
                    if 'Placement' in one:
                        instance_info['AvailabilityZones'] = one['Placement']['AvailabilityZone']
                    all_instacne.append(instance_info)
                    num_of_instance += 1
            print('Get instance from region %s, there are %s instance in this region' % (region, num_of_instance))

        return all_instacne


if __name__ == '__main__':
    parser = optparse.OptionParser()
    parser.add_option('-f', '--file',
                      dest='conf_name',
                      default='')
    parser.add_option('-o', '--output',
                      dest='output',
                      default='output.csv')
    options, remainder = parser.parse_args()
    if options.conf_name:
        result = []
        for account in account_parser(options.conf_name):
            aws_utils = AWSUtils(account['key_id'], account['secret_key'])
            result += aws_utils.get_instances()
        write_csv_file(options.output, result)
