# aws_utils

aws_utils is a tool to get all instances back


## Development Environment Requirements

1. Prepare python env. version 2.7 and pip

2. Install boto 
    
```console
$ pip install boto
```

3. Install boto3

```console
$ pip install boto3
```

## Run this script

1. Configure account info to account_group.json, you could check the example file named account_group_example.json
  
```console
  {
  "accounts": [
    {
      "AccesskeyID": "account_id_1",
      "Secretaccesskey": "secret_key_1"
    },
    {
      "AccesskeyID": "account_id_2",
      "Secretaccesskey": "secret_key_2"
    },
  ]
}
```
2. Get instances and output to a csv file

```console
$ ./python awsutils.py -f account_group.json -o my_instance.csv
```

3. Check the output csv file
